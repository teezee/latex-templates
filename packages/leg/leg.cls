% Latex-Class-File: leg.cls
% ============================================================================
% 2012/09/23 leg.cls V 0.1 (c) 2012 Thomas Zink
%
% ***************************************************************************
% Legal Notice:
% This code is offered as-is without any warranty either expressed or
% implied; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE! 
%
% This work is distributed under the LaTeX Project Public License (LPPL)
% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
% distributed and modified. A copy of the LPPL, version 1.3, is included
% in the base LaTeX documentation of all distributions of LaTeX released
% 2003/12/01 or later.
% Retain all contribution notices and credits.
% ** Modified files should be clearly indicated as such, including  **
% ** renaming them and changing author support contact information. **
% ***************************************************************************
%
% This is a class file for 'Leading Edge Games'-like book layout.
%
% Usage:
% Copy this file to local texmf/tex directory, or into your specific project.
% Then use with:
% \documentclass[Options]{leg}
%
% Options:
% none yet
% ============================================================================

\ProvidesClass{leg}[2012/09/23 v0.1 Leading Edge Games book class]
\NeedsTeXFormat{LaTeX2e}

\typeout{Class for writing a book in the style of Leading Edge Games publications}
% ============================================================================

\LoadClass[oneside]{book}

\RequirePackage{a4wide} % just fits better
\RequirePackage{colortbl} % need this for color'd tables (black headers)


\widowpenalty=1000
\clubpenalty=1000

% ------------------------------------------------------------------------------
% header / footer
% ------------------------------------------------------------------------------
\usepackage{fancyhdr}
\fancyhf{}
\chead{\leftmark}
\cfoot{\thepage}
\renewcommand{\headrulewidth}{5pt}
\renewcommand{\footrulewidth}{2pt}
% to have fancy chapter pages
\fancypagestyle{plain}{
	\chead{\leftmark}
	\cfoot{\thepage}
	\renewcommand{\headrulewidth}{5pt}
	\renewcommand{\footrulewidth}{2pt}
}
\pagestyle{fancy}

% ------------------------------------------------------------------------------
% chapter and section formats
% ------------------------------------------------------------------------------
\RequirePackage{xcolor}
\RequirePackage[explicit]{titlesec}

\titleformat{\chapter}[block] 
	{\normalfont\sffamily\hskip-2cm}
	{
		\colorbox{black}{
			\parbox[c][100pt][c]{100pt}{%
				\hfil\color{white}\Huge\scshape\thechapter\hfil
			}
		}
	}
	{10pt}
	{\parbox{\linewidth}{\huge\MakeUppercase{#1}}}

\titlespacing*{\chapter}
	{0pt}
	{-3ex plus 1ex minus 0.2ex}
	{2.3ex plus .2ex}

\titleformat{\section}[block]
	{\normalfont\sffamily\hskip-2cm}
	{
		\colorbox{black}{
			\parbox[c][20pt][c]{40pt}{%
				\hfil\color{white}\Large\thesection\hfil
			}
		}
	}
	{-0pt}
	{
		\Large\MakeUppercase{#1}\\
		\vskip-.9cm
		\color{black}\hskip-6pt\rule{1.1\linewidth}{4pt}
	}

%\titlespacing*{\section}
\titlespacing{\section}
	{0pt}
	{3ex plus 1ex minus 0.2ex}
	{-2.3ex plus .2ex}

% ------------------------------------------------------------------------------
% COMMANDS AND ENVIRONMENTS
% ------------------------------------------------------------------------------
\newcommand{\preface}[1]{
	{\bigskip\par\centering\normalfont\Large\MakeUppercase{#1}\par\bigskip}
	\addcontentsline{toc}{chapter}{#1}
	\markboth{#1}{}
}

\newenvironment{example}
     {\begin{quote}%
     \textbf{Example:}\itshape}
     {\end{quote}}

\endinput
