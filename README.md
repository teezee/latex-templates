# Templates

A collection of (mainly LaTeX) templates we use in our projects

## Contents

### Packages and Classes

- aircc.cls  
	A class for papers according to the formatting rules of AIRCC [http://airccse.org/]

- htwg.sty  
	A style file I use for all my papers, presentations, reports

- leg.cls
	A class that aims to be a modern interpretation of 'Leading Edge Games' (Aliens Adventure Game,
	Phoenix Command Systems, various RPGs) publication style.

- disythesis.cls
	A class for writing a thesis. Supports bachelor, master and phd.
